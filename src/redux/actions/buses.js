import api from '../../api';
import * as types from '../constants/actionTypes';

export const createBus = (data) => (dispatch) => {
  return api
    .post('/coach', data)
    .then((res) => {
      dispatch({
        type: types.CREATE_BUS,
        payload: res.data,
      });
      Promise.resolve(res.data);
    })
    .catch((err) => Promise.reject(err));
};

export const getBuses = () => (dispatch) => {
  return api
    .get('/coach')
    .then((res) => {
      dispatch({
        type: types.GET_BUSES,
        payload: res.data,
      });
    })
    .catch(console.log("error getBuses"));
};

export const updateBus = (_id, data) => (dispatch) => {
  return api
    .patch(`/coach/${_id}`, data)
    .then((res) => {
      dispatch({
        type: types.UPDATE_BUS,
        payload: res.data,
      });
      Promise.resolve(res.data);
    })
    .catch((err) => Promise.reject(err));
};

export const deleteBus = (_id) => (dispatch) => {
  api
    .delete(`/coach/${_id}`)
    .then((res) => {
      dispatch({
        type: types.DELETE_BUS,
        payload: _id,
      });
    })
    .catch(console.log("error deleteBus"));
};

export const paginationBuses = (query) => (dispatch) => {
  return api
    .get(`/coach/paginated/${query}`)
    .then((res) => {
      dispatch({
        type: types.GET_PAGINATION_BUSES,
        payload: res.data,
      });
    })
    .catch(console.log('buses paginated'));
};

export async function getCoachOwners() {
  try {
    const result = await api.get('/coach');
    let coachOwners = []
    result.data.results.forEach((coach) => {
      if (!coachOwners.includes(coach.coachOwner)) {
        coachOwners.push(coach.coachOwner);
      }
    })
    return coachOwners;
  } catch (error) {
    console.error(error);
  }
};

export async function getAnalyticsResult(owner) {
  try {
    const result = await api.get(
      `trips/analyst?owner=${owner}`
    );
    return result;
  } catch (error) {
    console.error(error);
  }
}
import * as types from '../constants/actionTypes';

const initialState = {
  results: [],
};

const busesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_BUS:
      state.results.push(action.payload);
      return { ...state };

    case types.GET_BUSES:
      return action.payload;

    case types.UPDATE_BUS:
      const bus = action.payload;
      const index = state.results.findIndex((b) => b._id === bus._id);
      state.results[index] = bus;
      return { ...state };

    case types.DELETE_BUS:
      const newState = state.results.filter((b) => b._id !== action.payload);

      return { results: newState };

    case types.GET_PAGINATION_BUSES:
      return action.payload;

    default:
      return state;
  }
};

export default busesReducer;

import {
  MDBCollapse, MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler, MDBNavItem,
  MDBNavLink
} from "mdbreact";
import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, withRouter } from "react-router-dom";
import { logout } from "../redux/actions/auth";

class TopNavigation extends Component {
  state = {
    collapse: false,
  };

  onClick = () => {
    this.setState({
      collapse: !this.state.collapse,
    });
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  };

  render() {
    return (
      <Router>
        <MDBNavbar color="light-blue" dark className="flexible-navbar" expand="md" scrolling>
          <MDBNavbarBrand
            href="/"
            onClick={() => {
              this.props.history.push("/");
            }}
          >
            <strong className="white-text">TRANG CHỦ</strong>
          </MDBNavbarBrand>
          <MDBNavbarToggler onClick={this.onClick} />
          <MDBCollapse isOpen={this.state.collapse} navbar>
            <MDBNavbarNav right>
              <MDBNavItem>
                <MDBNavLink
                  onClick={() => {
                    this.props.logout();
                    this.props.history.push("/");
                  }}
                  to="/"
                >
                  Đăng xuất
                </MDBNavLink>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBCollapse>
        </MDBNavbar>
      </Router>
    );
  }
}

export default withRouter(connect(null, { logout })(TopNavigation));

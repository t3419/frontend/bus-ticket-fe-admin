import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import React, { useState } from 'react';

function FileUploadPage(){
    const [pictures, setPictures] = useState([{
        data: [],
        url: ""
    }])

    const handleImageUpload = e => {
        const tempArr = [];
      
        [...e.target.files].forEach(file => {
          console.log("file >>> ", file);
      
          tempArr.push({
            data: file,
            url: URL.createObjectURL(file)
          });
      
          console.log("pictures >> ", pictures);
        });
      
        setPictures(tempArr);
      };

	return(
        <div className="post__pictures">
            <input type="file" multiple 
                onChange={handleImageUpload}
                accept="image/*"
            />

                {/* {pictures?.map(pic => (
                    <img src={pic.url}  alt=""/>
                ))} */}
                <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
                    {pictures?.map((item) => (
                    <ImageListItem key={item.url}>
                        <img
                        src={`${item.url}?w=164&h=164&fit=crop&auto=format`}
                        srcSet={`${item.url}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                        alt=""
                        loading="lazy"
                        />
                    </ImageListItem>
                    ))}
                </ImageList>
        </div>
	)
}

export default FileUploadPage
import Box from '@material-ui/core/Box';
import Button from "@material-ui/core/Button";
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import { Tabs } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import UpdateBus from "../../pages/UpdateBus";
import * as busActions from "../../redux/actions/buses";

const { TabPane } = Tabs;

class BusItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
  }

  setShow = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    const { bus, index } = this.props;
    const DetailInfo = () => {
      return (
        <Tabs defaultActiveKey="1" centered>
          <TabPane tab="Hình ảnh" key="1">
            <Box sx={{ overflowY: 'scroll' }}>
              <ImageList sx={{}} cols={1}>
              {bus?.image.map((item) => (
                <ImageListItem key={item}>
                  <img
                    src={`${item}?w=164&h=164&fit=crop&auto=format`}
                    srcSet={`${item}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                    alt={item.title}
                    loading="lazy"
                  />
                </ImageListItem>
              ))}
              </ImageList>
            </Box>
          </TabPane>
          <TabPane tab="Tiện ích" key="2">
            {bus?.description}
          </TabPane>
        </Tabs>
      );
    };

    return (
      <>
        <TableRow key={index}>
          <TableCell component="th" scope="row" align="center">
            {index + 1}
          </TableCell>
          <TableCell align="center">{bus.coachOwner}</TableCell>
          <TableCell align="center">{bus.numberSeat}</TableCell>
          <TableCell align="center">
            <Button
              variant="contained"
              color="primary"
              style={{ marginRight: "10px" }}
              onClick={() => this.setShow()}
            >
              Chi tiết
            </Button>
          </TableCell>

          <TableCell align="center" style={{ display: "flex", justifyContent: "center" }}>
            <Button
              variant="contained"
              color="secondary"
              startIcon={<DeleteIcon />}
              style={{ marginRight: "10px" }}
              onClick={() => {
                this.props.deleteBus(bus._id);
              }}
            >
              Xóa
            </Button>

            <UpdateBus bus={bus} />
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell colSpan={5} align="center">
            {this.state.show && <DetailInfo />}
          </TableCell>
        </TableRow>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    buses: state.buses,
  };
};

export default withRouter(connect(mapStateToProps, busActions)(BusItem));

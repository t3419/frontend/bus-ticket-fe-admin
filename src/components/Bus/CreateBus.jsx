import { Input } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import AddBoxIcon from "@material-ui/icons/AddBox";
import React, { useState } from "react";
import { connect } from "react-redux";
import * as busActions from "../../redux/actions/buses";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const typeSeats = [16,29,45]

function CreateBus({ createBus }) {
  const [input, setInput] = useState({ coachOwner: "", numberSeats: [], description: ""});
  const [open, setOpen] = React.useState(false);
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e) =>
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });

  const handleSubmit = () => {
    console.log('input:', input)
    // TODO: create with image
    input.numberSeats.forEach(numberSeat => {
      createBus({coachOwner: input.coachOwner, numberSeat: numberSeat, description: input.description});
    })
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" endIcon={<AddBoxIcon />} onClick={handleClickOpen}>
        Thêm xe khách
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Thêm xe khách</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="coachOwner"
            label="Tên nhà xe"
            type="text"
            name="coachOwner"
            value={input.coachOwner}
            fullWidth
            onChange={handleChange}
          />
          <FormControl sx={{ m: 1, width: 300 }} fullWidth margin="dense">
            <InputLabel id="demo-multiple-name-label">Số ghế</InputLabel>
            <Select
              multiple
              labelId="multiple-type-seat-label"
              id="numberSeats"
              name="numberSeats"
              value={input.numberSeats}
              onChange={handleChange}
              input={<OutlinedInput label="NameTypeSeat" />}
              MenuProps={MenuProps}
            >
              {typeSeats.map((nameTypeSeat) => (
                <MenuItem
                  key={nameTypeSeat}
                  value={nameTypeSeat}
                  // style={getStyles(nameTypeSeat, typeSeat, theme)}
                >
                  {nameTypeSeat}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            id="description"
            label="Tiện ích"
            type="text"
            name="description"
            fullWidth
            value={input.description}
            onChange={handleChange}
          />
          <br />
          <br />
          <InputLabel  htmlFor="images">Hình ảnh</InputLabel>
          <Input id="images" onChange={handleChange} type="file" placeholder="Chọn ảnh"/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Hủy
          </Button>
          <Button onClick={handleSubmit} color="primary" variant="contained">
            Xác nhận
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default connect(null, busActions)(CreateBus);

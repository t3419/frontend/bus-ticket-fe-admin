import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import AddBoxIcon from "@material-ui/icons/AddBox";
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import api from "../../../src/api/index";
import * as stationActions from "../../redux/actions/stations";

class CreateStation extends Component {
  constructor(props) {
    super(props);
    this.state = {name: "", address: "", province: "", district: "", 
                  open: false, listProvinces: [], listDistricts: []}
  }

  componentDidMount() {
    api.get("/province").then(result => {
      this.setState({listProvinces: result.data})
    })
  }

  setOpen = (value) => {
    this.setState({open: value})
  }
  setProvinces = (value) => {
    this.setState({listProvinces: value})
  }
  setDistricts = (value) => {
    this.setState({listDistricts: value})
  }

  handleClickOpen = () => {
    this.setOpen(true);
  };

  handleClose = () => {
    this.setOpen(false);
  };

  handleChange = (e) =>{
    this.setState({[e.target.name]: e.target.value})
  }

  handleChangeProvince = (event, value) => {
    this.setState({ province: value.name });
    const province = this.state.listProvinces.find(e=> e.name === value.name);
    
    api.get(`/province/${province.code}/district`).then(result => {
      this.setState({listDistricts: result.data.district})
    })
  };

  handleChangeDistrict = (event, value) => {
    this.setState({ district: value.nameWithType });
  }

  handleSubmit = () => {
    const input = {name: this.state.name, address: this.state.address, 
                    province: this.state.province, district: this.state.district}
    this.props.createStation(input);
    this.setOpen(false);
  };

  render() {
    return (
      <div>
        <Button variant="contained" endIcon={<AddBoxIcon />} onClick={this.handleClickOpen}>
          Thêm bến xe
        </Button>
        <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Thêm bến xe</DialogTitle>
          <DialogContent>
            <TextField
              style={{ width: "350px" }}
              autoFocus
              margin="dense"
              id="name"
              label="Tên bến xe"
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
              fullWidth
            />
            <Autocomplete
              style={{ width: "350px" }}
              id="acrossStation"
              name="acrossStation"
              options={this.state.listProvinces}
              onChange={this.handleChangeProvince}
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="standard"
                  label="Tỉnh/ Thành phố"
                />
              )}
            />
            <Autocomplete
              style={{ width: "350px" }}
              id="district"
              name="district"
              options={this.state.listDistricts}
              onChange={this.handleChangeDistrict}
              getOptionLabel={(option) => option.nameWithType}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="standard"
                  label="Huyện/ Quận"
                />
              )}
            />
            <TextField
              style={{ width: "350px" }}
              margin="dense"
              id="address"
              label="Địa chỉ"
              type="text"
              name="address"
              value={this.state.address}
              onChange={this.handleChange}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary" variant="contained">
              Hủy
            </Button>
            <Button onClick={this.handleSubmit} color="primary" variant="contained">
              Xác nhận
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    stations: state.stations,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createStation: (input) => {
      dispatch(stationActions.createStation(input))
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateStation));

import React from "react";
import { Route, Switch } from "react-router-dom";
import ProfilePage from "./pages/profile/myProfile/MyProfilePage";
import NotFoundPage from "./pages/NotFoundPage";
import EditProfile from "./pages/profile/editProfile/EditProfile";
import PaginationStations from "./pages/PaginationStations";
import UsersPage from "./pages/UsersPage";
import DashboardPage from "./pages/DashboardPage";
import BusesPage from "./pages/BusesPage";
import TripsPage from "./pages/TripsPage";
import CreateTrip from "./pages/CreateTrip";
import TicketsPage from "./pages/TicketsPage";


class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/admin/dashboard" exact component={DashboardPage} />
        <Route path="/admin/profile" exact component={ProfilePage} />
        <Route path="/admin/profile/edit-profile" exact component={EditProfile} />
        <Route path="/manager/stations" exact component={PaginationStations} />
        <Route path="/manager/buses" exact component={BusesPage} />
        <Route path="/manager/trips" exact component={TripsPage} />
        <Route path="/manager/trips/create-trip" exact component={CreateTrip} />
        <Route path="/manager/tickets" exact component={TicketsPage} />
        <Route path="/manager/users" exact component={UsersPage} />
        <Route path="*" component={NotFoundPage} />
      </Switch>
    );
  }
}

export default Routes;

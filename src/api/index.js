import axios from "axios";
let baseURL;

console.log("process.env.NODE_ENV", process.env.NODE_ENV);

switch (process.env.NODE_ENV) {
  case "development":
    baseURL = process.env.REACT_APP_API_DEV;
    break;

  case "production":
    baseURL = process.env.API_PRODUCTION;
    break;

  default:
    break;
}

const api = axios.create({ baseURL });

export default api;

import React, { Component } from "react";
import Pagination from "../components/Pagination";
import * as busActions from "../redux/actions/buses";
import { connect } from "react-redux";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import "./styles.css";
import CreateBus from "../components/Bus/CreateBus";
import BusItem from "../components/Bus/BusItem";

class BusesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalRecords: "",
      totalPages: "",
      pageLimit: "",
      currentPage: "",
      startIndex: "",
      endIndex: "",
    };
  }

  componentDidMount() {
    this.props.getBuses();
    this.setState({ totalRecords: this.props.buses.length });
  }

  showBuses = (buses) => {
    let result = null;
    if (buses.length > 0) {
      result = buses.map((bus, index) => {
        return <BusItem key={index} bus={bus} index={index} />;
      });
    }
    return result;
  };

  onChangePage = (data) => {
    this.setState({
      pageLimit: data.pageLimit,
      totalPages: data.totalPages,
      currentPage: data.page,
      startIndex: data.startIndex,
      endIndex: data.endIndex,
    });
  };

  render() {
    let { buses } = this.props;
    let { totalPages, currentPage, pageLimit, startIndex, endIndex } = this.state;
    let rowsPerPage = [];
    if (buses.results) {
      buses = buses.results
      rowsPerPage = buses.slice(startIndex, endIndex + 1);
    }
    return (
      <div className="section product_list_mng">
        <div className="container-fluid">
          <div className="box_product_control mb-15">
            <h1>Quản lý xe khách</h1>
            <CreateBus></CreateBus>
            <div className="row">
              <div className="col-xs-12 box_change_pagelimit mt-15">
                Hiển thị
                <select
                  className="form-control"
                  value={pageLimit}
                  onChange={(e) => this.setState({ pageLimit: parseInt(e.target.value) })}
                >
                  <option value={5}>5</option>
                  <option value={10}>10</option>
                  <option value={25}>25</option>
                  <option value={50}>50</option>
                  <option value={100}>100</option>
                </select>
                bến xe
              </div>
            </div>
          </div>

          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Số thứ tự</TableCell>
                  <TableCell align="center">Tên nhà xe</TableCell>
                  <TableCell align="center">Số ghế</TableCell>
                  <TableCell align="center">Hình ảnh, tiện ích</TableCell>
                  <TableCell align="center">Tùy chọn</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{this.showBuses(rowsPerPage)}</TableBody>
            </Table>
          </TableContainer>

          <div className="box_pagination">
            <div className="row">
              <div className="col-xs-12 box_pagination_info text-right">
                <p>
                  {buses.length} bến xe | Trang {currentPage}/{totalPages}
                </p>
              </div>
              <div className="col-xs-12 text-center ml-30">
                <Pagination
                  totalRecords={buses.length}
                  pageLimit={pageLimit || 5}
                  initialPage={1}
                  pagesToShow={5}
                  onChangePage={this.onChangePage}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    buses: state.buses,
  };
};

export default connect(mapStateToProps, busActions)(BusesPage);

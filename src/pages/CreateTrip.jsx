import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import AddBoxIcon from "@material-ui/icons/AddBox";
import Autocomplete from '@material-ui/lab/Autocomplete';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as stationActions from "../redux/actions/stations";
import * as tripActions from "../redux/actions/trips";
import * as busActions from "../redux/actions/buses";
// import 'react-day-picker/lib/style.css';

function CreateTrip(props) {
  const [input, setInput] = useState({
    fromStation: "",
    toStation: "",
    acrossStation: [],
    startTime: [],
    price: 0,
    coachId: "",
    description: ""
  });
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
    setInput({
      ...input,
      fromStation: props.stations.results[0]._id,
      toStation: props.stations.results[1]._id,
      coachId: props.buses.results[0]._id
    });
  };

  const handleClose = () => {
    setOpen(false);
    console.log('input:', input)
  };

  const resetInput = () => {
    setInput({
      fromStation: "",
      toStation: "",
      acrossStation: [],
      startTime: [],
      price: 0,
      coachId: "",
      description: ""
    });
  };

  const handleChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  }

  const handleChangeFromStation =(e, value) => {
    setInput({
      ...input,
      fromStation: value._id
    })
  }

  const handleChangeToStation =(e, value) => {
    setInput({
      ...input,
      toStation: value._id
    })
  }
  
  const handleAcrossStationChange = (event, values) => {
    let idAcrossStations = []
    values.forEach((item) => { 
      idAcrossStations.push(item._id)
    })
    setInput({
      ...input,
      acrossStation: idAcrossStations
    });
  }
  
  const handleChangeCoach =(e, value) => {
    setInput({
      ...input,
      coachId: value._id
    })
  }

  const addStartTime = e => {
    let now = new Date()
    let updatedStartTime = input.startTime.concat([now.toISOString().slice(0,16)])
    setInput({
      ...input,
      startTime: updatedStartTime
    });
  }

  const deleteStartTime = index => {
    let updatedStartTimes = input.startTime
    const idx = updatedStartTimes.indexOf(updatedStartTimes[index])
    if (idx > -1) {
      updatedStartTimes.splice(idx, 1);
    }
    setInput({
      ...input,
      startTime: updatedStartTimes
    });
  }
  const handleChangeStartTimeElement = (event, index) => {
    let updatedStartTime = input.startTime
    updatedStartTime[index] = event.target.value
    setInput({
      ...input,
      startTime: updatedStartTime
    });
  }

  const handleSubmit = (e) => {
    props.createTrip(input);
    setInput({
      fromStation: "",
      toStation: "",
      acrossStation: [],
      startTime: [],
      price: 0,
      coachId: "",
      description: ""
    });
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        endIcon={<AddBoxIcon />}
        onClick={handleClickOpen}
      >
        Thêm chuyến xe
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Thêm mới chuyến xe</DialogTitle>
        <DialogContent>
          <Autocomplete
            disableClearable
            id="fromStation"
            name="fromStation"
            options={props.stations.results}
            defaultValue={props.stations.results[0]}
            onChange={handleChangeFromStation}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Nơi đi"
              />
            )}
          />
          <Autocomplete
            disableClearable
            id="toStation"
            name="toStation"
            options={props.stations.results}
            defaultValue={props.stations.results[1]}
            onChange={handleChangeToStation}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Nơi đến"
              />
            )}
          />
          <Autocomplete
            multiple
            id="acrossStation"
            name="acrossStation"
            options={props.stations.results}
            onChange={handleAcrossStationChange}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Các bến đi qua"
              />
            )}
          />
          <Autocomplete
            disableClearable
            id="coachId"
            name="coachId"
            options={props.buses.results}
            defaultValue={props.buses.results[1]}
            onChange={handleChangeCoach}
            getOptionLabel={(elm) => ("Nhà xe " + elm.coachOwner + " loại " + elm.numberSeat + " ghế. Loại xe: " + elm.name)}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Chọn xe"
              />
            )}
          />
          <br />
          <br />
          <InputLabel style={{ width: "200px" }}>
            Thời gian các chuyến
          </InputLabel>
          <Button onClick={addStartTime} style={{display: "contents" }}>
            Thêm
          </Button>
          <br />
          {input.startTime.map((elm, index) => {
            return (
              <div key={index}>
                <TextField
                  type="datetime-local"
                  onChange={(event) => handleChangeStartTimeElement(event, index)}
                  value={elm}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  style={{ width: "200px" }}
                />
                <IconButton onClick={() => deleteStartTime(index)} aria-label="delete" size="small">
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </div>
            )
          })}
          <br />
          <br />
          <TextField
            type="number"
            id="price"
            label="Giá tiền"
            name="price"
            value={input.price}
            onChange={handleChange}
            style={{ width: "200px" }}
          />
          <br />
          <TextField
            type="string"
            id="description"
            label="Mô tả"
            name="description"
            value={input.description}
            onChange={handleChange}
            style={{ width: "400px", height: "100px" }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={resetInput} variant="contained">
            Nhập lại dữ liệu
          </Button>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Hủy
          </Button>
          <Button onClick={handleSubmit} color="primary" variant="contained">
            Xác nhận
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    stations: state.stations,
    buses: state.buses,
  };
};

export default connect(mapStateToProps, { ...stationActions, ...busActions, ...tripActions })(CreateTrip);

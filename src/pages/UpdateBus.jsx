import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { Input } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import * as busActions from "../redux/actions/buses";
import UpdateIcon from "@material-ui/icons/Update";

const typeSeats = [16,29,45]
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function UpdateBus(props) {
  // TODO: images
  const [input, setInput] = useState({ coachOwner: "", type: "", description: "", numberSeat: [] });
  const [open, setOpen] = useState(false);
  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e) =>
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });

  useEffect(() => {
    const { coachOwner, type, description, numberSeat } = props.bus;
    setInput({ coachOwner, type, description, numberSeat });
  }, [props.bus]);

  const onSubmit = (e) => {
    const { _id } = props.bus;
    props.updateBus(_id, input);
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" endIcon={<UpdateIcon />} onClick={handleClickOpen}>
        Cập nhật
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Cập nhật xe khách</DialogTitle>
        <DialogContent>
          <TextField
            disabled
            margin="dense"
            id="coachOwner"
            label="Tên nhà xe"
            type="text"
            name="coachOwner"
            value={input.coachOwner}
            fullWidth
          />
          <FormControl sx={{ m: 1, width: 300 }} fullWidth margin="dense">
            <InputLabel id="demo-multiple-name-label">Số ghế</InputLabel>
            <Select
              labelId="multiple-type-seat-label"
              id="numberSeat"
              name="numberSeat"
              value={input.numberSeat}
              onChange={handleChange}
              input={<OutlinedInput label="NameTypeSeat" />}
              MenuProps={MenuProps}
            >
              {typeSeats.map((nameTypeSeat) => (
                <MenuItem
                  key={nameTypeSeat}
                  value={nameTypeSeat}
                >
                  {nameTypeSeat}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            autoFocus
            margin="dense"
            id="description"
            label="Tiện ích"
            type="text"
            name="description"
            fullWidth
            value={input.description}
            onChange={handleChange}
            style={{ marginTop: "25px" }}
          />
          <br />
          <br />
          <InputLabel  htmlFor="images">Hình ảnh</InputLabel>
          <Input id="images" onChange={handleChange} type="file" placeholder="Chọn ảnh"/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Cancel
          </Button>
          <Button onClick={onSubmit} color="primary" variant="contained">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    buses: state.buses,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateBus: (id, data) => dispatch(busActions.updateBus(id, data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UpdateBus);

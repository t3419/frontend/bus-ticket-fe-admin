import React, {useState, useEffect} from "react";
import Link from '@material-ui/core/Link';
import * as busActions from "../redux/actions/buses";
import TextField from "@material-ui/core/TextField";
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Chart } from "react-google-charts";

const DashboardPage = (props) => {
  // TODO: cho chọn dự đoán chuyến xe tương ứng và gửi lên dữ liệu history_tickets của chuyến xe đó
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', 'accept': 'application/json' },
    body: JSON.stringify({ history_tickets: [] })
    // body: JSON.stringify({ history_tickets: [10, 12, 25, 20, 45, 60, 70, 12, 30, 12, 20, 45, 60, 70, 30, 12, 20, 45, 60, 70, 20, 30, 12, 20, 45, 60, 70, 20] })
  };
  const [sevenNextDays, setSevenNextDays] = useState([])
  const [coachOwners, setCoachOwners] = useState([])
  const [owner, setOwner] = useState([])

  useEffect(() => {
    busActions.getCoachOwners()
      .then((response) => {setCoachOwners(response)})
      .catch((error) => {console.log(error)});
  },[])

  const analyseNumberSeats = (owner) => {
    busActions.getAnalyticsResult(owner)
    .then((response) => {
      requestOptions.body = JSON.stringify({history_tickets: response.data.data})
      console.log("🚀 ~ file: DashboardPage.js ~ line 20 ~ .then ~ requestOptions", requestOptions)

      fetch("http://127.0.0.1:8000/", requestOptions)
        .then((response) => response.json())
        .then((responseJSON) => {
          console.log('responseJSON:', responseJSON)
          setSevenNextDays(responseJSON.seven_next_days)
        })
        .catch((error) => console.log(error));
    })
    .catch((error) => console.log(error));
  }
  
  const handleChangeOwner = (e, value) => {
    setOwner(value)
    analyseNumberSeats(value)
  }

  return (
    <React.Fragment>
      <h3>
      <Link href="https://analytics.google.com/analytics/web/#/realtime/rt-overview/a212854359w293497581p255356440/" underline="always">
        {'Đến trang phân tích web (Google Analytics)'}
      </Link>
      </h3>
      <div>
        <h3>Dự đoán số lượng vé cần cho 7 ngày tiếp theo của nhà xe:</h3>
        <Autocomplete
          disableClearable
          id="owner"
          name="owner"
          options={coachOwners}
          defaultValue={""}
          onChange={handleChangeOwner}
          getOptionLabel={(owner) => owner}
          style={{"width": "200px"}}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              label="Nhà xe"
            />
          )}
        />
        <br/>
        {sevenNextDays.length > 0 && (
          <Chart
            width={'100%'}
            height={'500'}
            chartType="Line"
            loader={<div>Loading Chart</div>}
            data={[
              [
                { type: 'date', label: 'Thứ và ngày' },
                'Số vé dự đoán',
                // 'Average hours of daylight',
              ],
              [new Date(2014, 12, 1), sevenNextDays[0]],
              [new Date(2014, 12, 2), sevenNextDays[1]],
              [new Date(2014, 12, 3), sevenNextDays[2]],
              [new Date(2014, 12, 4), sevenNextDays[3]],
              [new Date(2014, 12, 5), sevenNextDays[4]],
              [new Date(2014, 12, 6), sevenNextDays[5]],
              [new Date(2014, 12, 7), sevenNextDays[6]],
            ]}
            options={{
              chart: {
                title:
                  'Biểu đồ dự đoán số vé được đặt 7 ngày tiếp theo',
              },
              width: 900,
              height: 500,
              series: {
                // Gives each series an axis name that matches the Y-axis below.
                0: { axis: 'Số vé' },
                // 1: { axis: 'Daylight' },
              },
              axes: {
                // Adds labels to each axis; they don't have to match the axis names.
                y: {
                  Temps: { label: 'Số vé' },
                  // Daylight: { label: 'Daylight' },
                },
              },
            }}
            rootProps={{ 'data-testid': '2' }}
          />
        )}
        {/* {sevenNextDays.map((item, index) => 
            <h4 key={index}>Ngày thứ {index + 1}: {item}</h4>
        )} */}
      </div>
    </React.Fragment>
  );
};

export default DashboardPage;

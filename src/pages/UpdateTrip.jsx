import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import UpdateIcon from "@material-ui/icons/Update";
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as busActions from "../redux/actions/buses";
import * as stationActions from "../redux/actions/stations";
import * as tripActions from "../redux/actions/trips";


function UpdateTrip(props) {
  const [infoTrip, setInfoTrip] = useState({});
  const [coach, setCoach] = useState({});
  const [open, setOpen] = useState(false);
  const [startTime, setStartTime] = useState("")

  useEffect(() => {
    const { trip } = props;
    setInfoTrip(trip);
    setCoach(trip.coach)
  }, []);
  
  const handleClickOpen = () => {
    setStartTime(infoTrip.startTime.slice(0,16))
    console.log("🚀 ~ file: UpdateTrip.jsx ~ line 36 ~ handleClickOpen ~ startTime", startTime)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e) => {
    setInfoTrip({
      ...infoTrip,
      [e.target.name]: e.target.value,
    });
    console.log('infoTrip:', infoTrip)
  }

  const handleChangeStartTime = (event, index) => {
    setInfoTrip({
      ...infoTrip,
      startTime: event.target.value
    });
  }

  const handleChangeCoach = (e, value) => {
    console.log("🚀 ~ file: UpdateTrip.jsx ~ line 53 ~ handleChangeCoach ~ value", value)
    
    setInfoTrip({
      ...infoTrip,
      coachId: value._id
    })
  }

  const handleAcrossStationChange = (event, values) => {
    let idAcrossStations = []
    values.forEach((item) => { 
      idAcrossStations.push(item._id)
     })
    setInfoTrip({
      ...infoTrip,
      acrossStation: idAcrossStations
    });
  }

  const handleSubmit = (e) => {
    const { trip } = props;
    console.log('infoTrip:', infoTrip)
    props.updateTrip(trip._id, infoTrip);
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        endIcon={<UpdateIcon />}
        onClick={handleClickOpen}
      >
        Cập nhật
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Cập nhật chuyến xe</DialogTitle>
        <DialogContent>
          <FormControl>
            <InputLabel style={{ width: "200px" }} id="fromStation">
              Ga xuất phát
            </InputLabel>
            <Select
              disabled
              style={{ width: "200px" }}
              labelId="fromStation"
              id="fromStation"
              name="fromStation"
              value={infoTrip.fromStation ? infoTrip.fromStation._id : ""}
              onChange={handleChange}
            >
              {props.stations.results.map((elm, index) => {
                return (
                  <MenuItem key={index} value={elm._id}>
                    {elm.name}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <FormControl>
            <InputLabel style={{ width: "200px" }} id="toStation">
              Ga đến
            </InputLabel>
            <Select
              disabled
              style={{ width: "200px" }}
              labelId="toStation"
              id="toStation"
              name="toStation"
              value={infoTrip.toStation ? infoTrip.toStation._id : ""}
              onChange={handleChange}
            >
              {props.stations.results.map((elm, index) => {
                return (
                  <MenuItem key={index} value={elm._id}>
                    {elm.name}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <br />
          <Autocomplete
            multiple
            id="acrossStation"
            name="acrossStation"
            options={props.stations.results}
            onChange={handleAcrossStationChange}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Ga đi qua"
              />
            )}
          />
          <Autocomplete
            disableClearable
            // style={{ width: "350px" }}
            id="coach"
            name="coach"
            defaultValue={coach}
            options={props.buses.results}
            onChange={handleChangeCoach}
            getOptionLabel={(elm) => ("Nhà xe " + elm.coachOwner + " loại " + elm.numberSeat + " ghế. Loại xe: " + elm.name)}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Chọn xe"
              />
            )}
          />
          <br />
          <InputLabel style={{ width: "200px" }}>
            Thời gian
          </InputLabel>
          <TextField
            type="datetime-local"
            defaultValue={startTime}
            onChange={handleChangeStartTime}
            InputLabelProps={{
              shrink: true,
            }}
            style={{ width: "200px" }}
          />
          <br />
          <br />
          <TextField
            type="number"
            id="price"
            label="Giá tiền"
            name="price"
            value={infoTrip.price}
            onChange={handleChange}
            style={{ width: "200px" }}
          />
          <br />
          <br />
          <TextField
            type="string"
            id="description"
            label="Mô tả"
            name="description"
            value={infoTrip.description}
            onChange={handleChange}
            style={{ width: "400px", height: "100px" }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Hủy
          </Button>
          <Button onClick={handleSubmit} color="primary" variant="contained">
            Xác nhận
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    stations: state.stations,
    buses: state.buses,
  };
};

export default connect(mapStateToProps, { ...stationActions, ...busActions, ...tripActions })(UpdateTrip);

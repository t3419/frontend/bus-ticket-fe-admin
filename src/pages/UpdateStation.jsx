import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Autocomplete from '@material-ui/lab/Autocomplete';
import UpdateIcon from "@material-ui/icons/Update";
import * as stationActions from "../redux/actions/stations";
import api from "../../src/api/index";

function UpdateStation(props) {
  const [input, setInput] = useState({ image: "", name: "", address: "", province: "", district: "" });
  const [listDistricts, setListDistricts] = useState([])
  const [districtObject, setDistrictObject] = useState({})
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const { name, province, address, district } = props.station;
    setInput({ name, province, address, district });
    
    api.get("/province").then(result => {
      const listProvinces = result.data
      const provinceObject = listProvinces.find(e=> e.name === province);
      api.get(`/province/${provinceObject.code}/district`).then(result => {
        setListDistricts(result.data.district)
      })
    })
  }, [props.station]);

  const handleClickOpen = () => {
    const { district } = props.station;
    setDistrictObject(listDistricts.find(e=> e.nameWithType === district))
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e) =>
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });

  const handleChangeDistrict = (event, value) => {
    setInput({
      ...input,
      district: value.nameWithType,
    });
  }

  const onSubmit = (e) => {
    const { _id } = props.station;
    console.log("🚀 ~ file: UpdateStation.jsx ~ line 58 ~ onSubmit ~ _id", _id)
    console.log('input:', input)
    props.updateStation(_id, input);
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" endIcon={<UpdateIcon />} onClick={handleClickOpen}>
        Cập nhật
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Cập nhật bến xe</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Tên bến xe"
            type="text"
            name="name"
            value={input.name}
            onChange={handleChange}
            fullWidth
          />
          <TextField
            margin="dense"
            id="address"
            label="Địa chỉ"
            type="text"
            name="address"
            value={input.address}
            onChange={handleChange}
            fullWidth
            style={{ marginTop: "25px" }}
          />
          <Autocomplete
            disableClearable
            style={{ width: "350px" }}
            id="district"
            name="district"
            defaultValue={districtObject}
            options={listDistricts}
            onChange={handleChangeDistrict}
            getOptionLabel={(option) => option.nameWithType}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Huyện/ Quận"
              />
            )}
          />
          <TextField
            disabled
            margin="dense"
            id="province"
            label="Tỉnh/ Thành phố"
            type="text"
            name="province"
            fullWidth
            value={input.province}
            onChange={handleChange}
            style={{ marginTop: "25px" }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained">
            Cancel
          </Button>
          <Button onClick={onSubmit} color="primary" variant="contained">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    stations: state.stations,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateStation: (id, data) => dispatch(stationActions.updateStation(id, data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UpdateStation);
